﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Property Name="PetranWayTag:-:PetranWayAutoDocPrinterReportComboTag:-:PetranWay_Documentation Reports.lvlib:Project Report.lvclass::PetranWay_Documentation Reports.lvlib:BitBucket Wiki Markdown.lvclass" Type="Str">C:\PetranWay\Wikis\Software Module Template\Developer Resources\APIs\Software Module Template NIPKG VIP Project Documentation.md</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Build" Type="Folder">
			<Item Name="Build Spec" Type="Folder">
				<Item Name="NIPKG_LV" Type="Folder">
					<Item Name="SubSpecs" Type="Folder">
						<Item Name="SrcDist.ini" Type="Document" URL="../NIPKG_LV/SubSpecs/SrcDist.ini"/>
					</Item>
					<Item Name="Commit Behavior.ini" Type="Document" URL="../NIPKG_LV/Commit Behavior.ini"/>
					<Item Name="Software Module Template.ini" Type="Document" URL="../NIPKG_LV/Software Module Template.ini"/>
				</Item>
				<Item Name="VIP" Type="Folder">
					<Item Name="Commit Behavior.ini" Type="Document" URL="../VIP/Commit Behavior.ini"/>
					<Item Name="VIPackage.ini" Type="Document" URL="../VIP/VIPackage.ini"/>
				</Item>
				<Item Name="Software Module Definition.ini" Type="Document" URL="../Software Module Definition.ini"/>
				<Item Name="VIPackage.vipb" Type="Document" URL="../VIPackage.vipb"/>
			</Item>
			<Item Name="License" Type="Folder">
				<Item Name="License.rtf" Type="Document" URL="../../License/License.rtf"/>
			</Item>
		</Item>
		<Item Name="Source" Type="Folder" URL="../../../Source">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="PetranWay_SrcDist BldSpec Utils.lvlib" Type="Library" URL="/&lt;userlib&gt;/_PetranWay/SrcDist BldSpec Utils/Source/PetranWay_SrcDist BldSpec Utils.lvlib"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Compare Two Paths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="JDP Timestamp.lvlib" Type="Library" URL="/&lt;vilib&gt;/JDP Science/JDP Science Common Utilities/Timestamp/JDP Timestamp.lvlib"/>
				<Item Name="JDP Utility.lvlib" Type="Library" URL="/&lt;vilib&gt;/JDP Science/JDP Science Common Utilities/JDP Utility.lvlib"/>
				<Item Name="JSONtext.lvlib" Type="Library" URL="/&lt;vilib&gt;/JDP Science/JSONtext/JSONtext.lvlib"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/Numeric/LVNumericRepresentation.ctl"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_XML.lvlib" Type="Library" URL="/&lt;vilib&gt;/xml/NI_XML.lvlib"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="DOMUserDefRef.dll" Type="Document" URL="DOMUserDefRef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="SrcDist" Type="Source Distribution">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{289BD885-E641-4E72-BE27-35FDE9617151}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">SrcDist</Property>
				<Property Name="Bld_excludedDirectory[0]" Type="Path">vi.lib</Property>
				<Property Name="Bld_excludedDirectory[0].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[1]" Type="Path">resource/objmgr</Property>
				<Property Name="Bld_excludedDirectory[1].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[2]" Type="Path">/C/ProgramData/National Instruments/InstCache/17.0</Property>
				<Property Name="Bld_excludedDirectory[3]" Type="Path">/C/Users/labview/Documents/LabVIEW Data/2017(32-bit)/ExtraVILib</Property>
				<Property Name="Bld_excludedDirectory[4]" Type="Path">instr.lib</Property>
				<Property Name="Bld_excludedDirectory[4].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[5]" Type="Path">user.lib</Property>
				<Property Name="Bld_excludedDirectory[5].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectoryCount" Type="Int">6</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/builds/NI_AB_PROJECTNAME/SrcDist</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{B7CD5AC8-A3FE-458A-9EB9-2885D53EFB2C}</Property>
				<Property Name="Bld_userLogFile" Type="Path">/C/builds/Component Template NIPKG VIP/SrcDist _log.txt</Property>
				<Property Name="Bld_version.build" Type="Int">52</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/SrcDist</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/SrcDist/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].destName" Type="Str">License</Property>
				<Property Name="Destination[2].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/SrcDist/License</Property>
				<Property Name="Destination[2].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Source[0].itemID" Type="Str">{EECD0D4A-4DEC-4299-89D5-5D05FF05BCD1}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[1].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Source</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">Container</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[2].itemID" Type="Ref"></Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
			</Item>
			<Item Name="NIPKG" Type="{E661DAE2-7517-431F-AC41-30807A3BDA38}">
				<Property Name="NIPKG_license" Type="Ref"></Property>
				<Property Name="NIPKG_releaseNotes" Type="Str">


&lt;Upgrade Information&gt;

   Upgraded:    petranway-custominstallsteplauncher-nipkg-lv
           From:    1.1.0-25
              To:     1.1.0-29

   Upgraded:    pw-reuse-srcdst-bldspec-utils-nipkg-lv
           From:    1.0.0-11
              To:     1.0.0-12

&lt;/Upgrade Information&gt;</Property>
				<Property Name="PKG_actions.Count" Type="Int">0</Property>
				<Property Name="PKG_autoIncrementBuild" Type="Bool">false</Property>
				<Property Name="PKG_autoSelectDeps" Type="Bool">false</Property>
				<Property Name="PKG_buildNumber" Type="Int">25</Property>
				<Property Name="PKG_buildSpecName" Type="Str">NIPKG</Property>
				<Property Name="PKG_dependencies.Count" Type="Int">3</Property>
				<Property Name="PKG_dependencies[0].Enhanced" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[0].MaxVersion" Type="Str"></Property>
				<Property Name="PKG_dependencies[0].MaxVersionInclusive" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[0].MinVersion" Type="Str">18.0.0.49152-0+f0</Property>
				<Property Name="PKG_dependencies[0].MinVersionType" Type="Str">Inclusive</Property>
				<Property Name="PKG_dependencies[0].NIPKG.DisplayName" Type="Str">Package Building Support for LabVIEW 2017 (32-bit)</Property>
				<Property Name="PKG_dependencies[0].Package.Name" Type="Str">ni-package-builder-labview-2017-support-x86</Property>
				<Property Name="PKG_dependencies[0].Package.Section" Type="Str">Add-Ons</Property>
				<Property Name="PKG_dependencies[0].Package.Synopsis" Type="Str">Allows building packages from LabVIEW 2017 (32-bit)</Property>
				<Property Name="PKG_dependencies[0].Relationship" Type="Str">Required Dependency</Property>
				<Property Name="PKG_dependencies[0].Type" Type="Str">NIPKG</Property>
				<Property Name="PKG_dependencies[1].Enhanced" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[1].MaxVersion" Type="Str"></Property>
				<Property Name="PKG_dependencies[1].MaxVersionInclusive" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[1].MinVersion" Type="Str">1.1.0-29</Property>
				<Property Name="PKG_dependencies[1].MinVersionType" Type="Str">Inclusive</Property>
				<Property Name="PKG_dependencies[1].NIPKG.DisplayName" Type="Str">Custom Install Step Launcher - NIPKG - LV</Property>
				<Property Name="PKG_dependencies[1].Package.Name" Type="Str">petranway-custominstallsteplauncher-nipkg-lv</Property>
				<Property Name="PKG_dependencies[1].Package.Section" Type="Str">Infrastructure</Property>
				<Property Name="PKG_dependencies[1].Package.Synopsis" Type="Str">Launches the VI responsible for custom install \ unsinstall behavior.</Property>
				<Property Name="PKG_dependencies[1].Relationship" Type="Str">Required Dependency</Property>
				<Property Name="PKG_dependencies[1].Type" Type="Str">NIPKG</Property>
				<Property Name="PKG_dependencies[2].Enhanced" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[2].MaxVersion" Type="Str"></Property>
				<Property Name="PKG_dependencies[2].MaxVersionInclusive" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[2].MinVersion" Type="Str">1.0.0-11</Property>
				<Property Name="PKG_dependencies[2].MinVersionType" Type="Str">Inclusive</Property>
				<Property Name="PKG_dependencies[2].NIPKG.DisplayName" Type="Str">PetranWay SrcDst BldSpec Utils - NIPKG - LV</Property>
				<Property Name="PKG_dependencies[2].Package.Name" Type="Str">pw-reuse-srcdst-bldspec-utils-nipkg-lv</Property>
				<Property Name="PKG_dependencies[2].Package.Section" Type="Str">Infrastructure</Property>
				<Property Name="PKG_dependencies[2].Package.Synopsis" Type="Str">These are common functions used in creating a LabVIEW source distribution build specification.</Property>
				<Property Name="PKG_dependencies[2].Relationship" Type="Str">Required Dependency</Property>
				<Property Name="PKG_dependencies[2].Type" Type="Str">NIPKG</Property>
				<Property Name="PKG_dependencies[3].Enhanced" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[3].MaxVersion" Type="Str"></Property>
				<Property Name="PKG_dependencies[3].MaxVersionInclusive" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[3].MinVersion" Type="Str">1.6.0-19</Property>
				<Property Name="PKG_dependencies[3].MinVersionType" Type="Str">Inclusive</Property>
				<Property Name="PKG_dependencies[3].NIPKG.DisplayName" Type="Str">Software Module Builder - NIPKG - LV</Property>
				<Property Name="PKG_dependencies[3].Package.Name" Type="Str">petranway-softwaremodule-builder-nipkg-lv</Property>
				<Property Name="PKG_dependencies[3].Package.Section" Type="Str">Infrastructure</Property>
				<Property Name="PKG_dependencies[3].Package.Synopsis" Type="Str">Software Module builder turns source into exports.</Property>
				<Property Name="PKG_dependencies[3].Relationship" Type="Str">Required Dependency</Property>
				<Property Name="PKG_dependencies[3].Type" Type="Str">NIPKG</Property>
				<Property Name="PKG_description" Type="Str"></Property>
				<Property Name="PKG_destinations.Count" Type="Int">3</Property>
				<Property Name="PKG_destinations[0].ID" Type="Str">{77809D6F-B364-418E-BA12-644B09F4DD84}</Property>
				<Property Name="PKG_destinations[0].Subdir.Directory" Type="Str">Software Module Template NIPKG VIP</Property>
				<Property Name="PKG_destinations[0].Subdir.Parent" Type="Str">{BC833C7E-5DDF-4CB0-A613-B56411E1262F}</Property>
				<Property Name="PKG_destinations[0].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[1].ID" Type="Str">{BC833C7E-5DDF-4CB0-A613-B56411E1262F}</Property>
				<Property Name="PKG_destinations[1].Subdir.Directory" Type="Str">PetranWay</Property>
				<Property Name="PKG_destinations[1].Subdir.Parent" Type="Str">root_0</Property>
				<Property Name="PKG_destinations[1].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[2].ID" Type="Str">{F2010E8C-313A-41C9-92F6-C4263FFA5F03}</Property>
				<Property Name="PKG_destinations[2].Subdir.Directory" Type="Str">Source</Property>
				<Property Name="PKG_destinations[2].Subdir.Parent" Type="Str">{77809D6F-B364-418E-BA12-644B09F4DD84}</Property>
				<Property Name="PKG_destinations[2].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[3].ID" Type="Str">{BC833C7E-5DDF-4CB0-A613-B56411E1262F}</Property>
				<Property Name="PKG_destinations[3].Subdir.Directory" Type="Str">Tool Chain Support</Property>
				<Property Name="PKG_destinations[3].Subdir.Parent" Type="Str">root_0</Property>
				<Property Name="PKG_destinations[3].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[4].ID" Type="Str">{94424ACA-B9A1-43F5-B665-0D4C2B382EE5}</Property>
				<Property Name="PKG_destinations[4].Subdir.Directory" Type="Str">LabVIEW 2017</Property>
				<Property Name="PKG_destinations[4].Subdir.Parent" Type="Str">{785806D4-4FF4-469F-A067-6785536A2206}</Property>
				<Property Name="PKG_destinations[4].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[5].ID" Type="Str">{DA3515BE-921A-42B6-BB7C-6D6FC317C5E4}</Property>
				<Property Name="PKG_destinations[5].Subdir.Directory" Type="Str">Component Builder</Property>
				<Property Name="PKG_destinations[5].Subdir.Parent" Type="Str">{25ED02F4-37C6-4646-BF87-4071307FF213}</Property>
				<Property Name="PKG_destinations[5].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[6].ID" Type="Str">{EFC01A7E-1FA7-43C5-87FC-003C6EA4C968}</Property>
				<Property Name="PKG_destinations[6].Subdir.Directory" Type="Str">user.lib</Property>
				<Property Name="PKG_destinations[6].Subdir.Parent" Type="Str">{94424ACA-B9A1-43F5-B665-0D4C2B382EE5}</Property>
				<Property Name="PKG_destinations[6].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[7].ID" Type="Str">{FF3399FF-CEAA-4469-B1E4-8602DBB518BC}</Property>
				<Property Name="PKG_destinations[7].Subdir.Directory" Type="Str">Component Prototype</Property>
				<Property Name="PKG_destinations[7].Subdir.Parent" Type="Str">{26D9F558-F626-4917-8197-C37CC53E0263}</Property>
				<Property Name="PKG_destinations[7].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_displayName" Type="Str">Software Module Template NIPKG VIP - NIPKG - LV</Property>
				<Property Name="PKG_displayVersion" Type="Str"></Property>
				<Property Name="PKG_homepage" Type="Str">https://bitbucket.org/ChrisCilino/software-module-template-nipkg-vip</Property>
				<Property Name="PKG_lvrteTracking" Type="Bool">false</Property>
				<Property Name="PKG_maintainer" Type="Str">PetranWay &lt;christopher.cilino@petranway.com&gt;</Property>
				<Property Name="PKG_output" Type="Path">/C/builds/NI_AB_PROJECTNAME/NIPKG</Property>
				<Property Name="PKG_packageName" Type="Str">petranway-softwaremodule-template-nipkg-vip-nipkg-lv</Property>
				<Property Name="PKG_ProviderVersion" Type="Int">1810</Property>
				<Property Name="PKG_section" Type="Str">Infrastructure</Property>
				<Property Name="PKG_shortcuts.Count" Type="Int">0</Property>
				<Property Name="PKG_sources.Count" Type="Int">1</Property>
				<Property Name="PKG_sources[0].Destination" Type="Str">{F2010E8C-313A-41C9-92F6-C4263FFA5F03}</Property>
				<Property Name="PKG_sources[0].ID" Type="Ref">/My Computer/Build Specifications/SrcDist</Property>
				<Property Name="PKG_sources[0].Type" Type="Str">Build</Property>
				<Property Name="PKG_sources[1].Destination" Type="Str">{FF3399FF-CEAA-4469-B1E4-8602DBB518BC}</Property>
				<Property Name="PKG_sources[1].ID" Type="Ref">/My Computer/Build Specifications/SrcDist</Property>
				<Property Name="PKG_sources[1].Type" Type="Str">Build</Property>
				<Property Name="PKG_synopsis" Type="Str">Template for a software module that creates exports to packages.</Property>
				<Property Name="PKG_version" Type="Str">1.1.0</Property>
			</Item>
		</Item>
	</Item>
</Project>
